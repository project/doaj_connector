<?php
/**
 * @file
 *  Submit content to DOAJ
 */


/**
 * DOAJ connector settings
 */
function doaj_connector_admin_settings($form, &$form_state) {
  drupal_set_title(t('DOAJ settings'));
  $form['access_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Access Settings'),
    '#collapsible' => TRUE,
  );

  $form['access_settings']['doaj_connector_access_apikey'] = array(
    '#type' => 'textfield',
    '#title' => t('API Key'),
    '#description' => t('To get your API Key, please log in to the DOAJ website, go to the top right of the page and click your username. If you have generated an API key already, it will appear under your name. If not, click the Generate API Key button.'),
    '#default_value' => variable_get('doaj_connector_access_apikey', ''),
  );
  
  $form['sitespecific_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Site-specific Settings'),
    '#collapsible' => TRUE,
  );

  $form['sitespecific_settings']['doaj_connector_sitespecific_include_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name of your site-specific include file'),
    '#field_suffix' => ' .sitespecific.inc',
    '#description' => t('This module requires a PHP include file that has been customized for your site. Please see example.sitespecific.inc file in the module directory.as an example and follow the directions given in that file.'),
    '#default_value' => variable_get('doaj_connector_sitespecific_include_name', ''),
  );
  $form['connection_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('API Settings'),
    '#collapsible' => TRUE,
  );
  $form['connection_settings']['doaj_connector_connection_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Endpoint URL'),
    '#default_value' => variable_get('doaj_connector_connection_url', 'https://doaj.org/api/v1'),
    '#description' => t('Normally this should be https://doaj.org/api/v1'),
  );
  $form['content_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Content Settings'),
    '#collapsible' => TRUE,
  );
  $node_types = node_type_get_names();
  $form['content_settings']['doaj_connector_content_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Enabled content types'),
    '#description' => t('Choose the types on which you would like to enable the DOAJ connect functionality. Choose none to enable it on all content types.'),
    '#options' => $node_types,
    '#default_value' => variable_get('doaj_connector_content_types', array()),
  );
  return system_settings_form($form);
}

/**
 * Build the form that uploads the content
 */
function doaj_connector_page_connect_form($form, &$form_state, $node) {
  drupal_set_title(t('DOAJ'));
  $form['nid'] = array(
    '#type' => 'value',
    '#value' => $node->nid,
  );
  $update_necessary=FALSE;
  $update_possible=TRUE;

  $doaj_info=_doaj_connector_get_doaj_info($node->nid);

  $form['info'] = array(
    '#type' => 'fieldset',
    '#title' => t('Info'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  if ($doaj_info['availability']==DOAJ_CONNECTOR_AVAILABILITY_UPLOADED) {
    $form['info']['status'] = array(
      '#markup' => t('Current Status: Uploaded') . '<br />',
    );
    if (!$node->status) {
      $form['info']['status2'] = array(
        '#markup' => t('Node Status: Unpublished (Update necessary)') . '<br />',
      );
      $update_necessary=TRUE;
    }
  }
  elseif ($doaj_info['availability']==DOAJ_CONNECTOR_AVAILABILITY_REMOVED) {
    $form['info']['status'] = array(
      '#markup' => t('Current Status: Deleted') . '<br />',
    );
    if ($node->status) {
      $form['info']['status2'] = array(
        '#markup' => t('Node Status: Published (Update necessary)') . '<br />',
      );
      $update_necessary=TRUE;
    }
  }
  else {
    $form['info']['status'] = array(
      '#markup' => t('Current Status: Not uploaded yet') . '<br />',
    );
    $update_necessary=TRUE;
  }
  if ($doaj_info['article_id']!='') {
    $form['info']['article_id'] = array(
      '#markup' => t('DOAJ Article ID: !link', array('!link' => l($doaj_info['article_id'], 'https://doaj.org/article/' . $doaj_info['article_id']))) . '<br />',
    );
  }
  $form['info']['json_content'] = array(
    '#markup' => t('DOAJ JSON file to be uploaded: !link', array('!link' => l(t('doaj_connector.json'), 'node/' . $node->nid . '/doaj_connector.json'))) . '<br />',
  );
  if ($doaj_info['availability']==DOAJ_CONNECTOR_AVAILABILITY_UPLOADED) {
    $node_json_warnings=array();
    $node_json=_doaj_connector_get_node_json($node, $node_json_warnings);
    if ($node_json_warnings) {
      $warning_counter=0;
      foreach ($node_json_warnings as $node_json_warning) {
        $form['info']['warning_' . $warning_counter] = array(
          '#markup' => $node_json_warning . '<br />',
        );
        $warning_counter++;
      }
    }
    if ($node_json=='') {
      $form['info']['failed_node_json'] = array(
        '#markup' => t('DOAJ JSON file could not be generated.') . '<br />',
      );
      $update_possible=FALSE;
    }
    if ($doaj_info['node_json']!=$node_json) {
      $form['info']['old_json_content'] = array(
        '#markup' => t('DOAJ JSON file previously uploaded: !link (different)', array('!link' => l(t('doaj_connector.old.json'), 'node/' . $node->nid . '/doaj_connector.old.json'))) . '<br />',
      );
      $update_necessary=TRUE;
    }
  }
  // check whether last operation was successful
  if (!$update_necessary) {
    $result = db_query_range('SELECT nid, result_code FROM {doaj_connector_node_log} WHERE nid=:nid ORDER BY upload_timestamp DESC', 0, 1, array(':nid' => $node->nid));
    foreach ($result as $record) {
      if (($record->result_code!=200) && ($record->result_code!=201) && ($record->result_code!=204)) {
        $form['info']['last_operation'] = array(
          '#markup' => t('Last operation was not successful (Update necessary)') . '<br />',
        );
        $update_necessary=TRUE;
      }
    }
  }
  if ($update_possible) {
    $form['update'] = array(
      '#type' => 'fieldset',
      '#title' => t('Update'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    );
    if ($update_necessary) {
      $form['update']['#description'] = t('Update is necessary.');
    }

    $status_text=$node->status ? t('published') : t('unpublished');
    $form['update']['availability'] = array(
      '#type' => 'checkbox',
      '#title' => t('This content is available (Node status: @status)', array('@status' => $status_text)),
      '#default_value' => $node->status ? TRUE : FALSE,
    );
    $form['update']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Update information on DOAJ'),
      '#submit' => array('doaj_connector_page_connect_form_upload_submit'),
    );
  }

  $header = array(t('Time'), t('User'), t('Operation'), t('Result'), t('Message'));
  $rows = array();
  $result = db_query('SELECT * FROM {doaj_connector_node_log} WHERE nid=:nid ORDER BY upload_timestamp DESC', array(':nid' => $node->nid));
  foreach ($result as $record) {
    $username="";
    if ($account=user_load($record->upload_uid)) {
      $username=check_plain(format_username($account));
    }
    if ($username=="") {
      $username=t('User !uid', array('!uid' => $record->upload_uid));
    }
    if ($record->availability==DOAJ_CONNECTOR_AVAILABILITY_UPLOADED) {
      $operation=t('Upload');
    }
    elseif ($record->availability==DOAJ_CONNECTOR_AVAILABILITY_REMOVED) {
      $operation=t('Remove');
    }
    else {
      $operation=t('None');
    }
    $rows[] = array(
      format_date($record->upload_timestamp, 'short'),
      l($username, '/user/' . $record->upload_uid),
      $operation,
      $record->result_code,
      $record->result_text,
    );
  }
  if (count($rows)) {
    $form["log"] = array(
      '#type' => 'fieldset',
      '#title' => t('Log'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    );
    $form['log']['table'] = array(
      '#markup' => check_plain('') . '<br />' . theme('table', array('header' => $header, 'rows' => $rows)) . '<br />',
    );
  }

  return $form;
}

/**
 * Button submit function to upload content
 */
function doaj_connector_page_connect_form_upload_submit($form, &$form_state) {
  global $user;
  $nid=$form_state['values']['nid'];
  // checkbox whether admin wants to upload or delete
  $availability=$form_state['values']['availability'] ? DOAJ_CONNECTOR_AVAILABILITY_UPLOADED : DOAJ_CONNECTOR_AVAILABILITY_REMOVED;
  $node=node_load($nid);
  if (!$node) {
    drupal_set_message(t('Failed to load node'), 'error');
    return;
  }
  $apikey=trim(variable_get('doaj_connector_access_apikey', ''));
  $url=variable_get('doaj_connector_connection_url', 'https://doaj.org/api/v1');
  if ($apikey=="") {
    drupal_set_message(t('Please configure the API Key.'), 'error');
    return;
  }
  if ($url=="") {
    drupal_set_message(t('Please configure the Endpoint URL.'), 'error');
    return;
  }
  // get doaj info of the node, including the DOAJ article_id
  $doaj_info = _doaj_connector_get_doaj_info($node->nid);

  // determine what needs to be done (GET, PUT, POST, DELETE)
  $op='';
  if ($availability==DOAJ_CONNECTOR_AVAILABILITY_REMOVED) {
    if ($doaj_info['availability']!=DOAJ_CONNECTOR_AVAILABILITY_UPLOADED) {
      drupal_set_message(t('It seems nothing needs to be done: the node is currently not uploaded.'));
      return;
    }
    $op='DELETE';
  }
  else {
    if ($doaj_info['availability']!=DOAJ_CONNECTOR_AVAILABILITY_UPLOADED) {
      $op='POST';
    }
    else {
      $op='PUT';
    }
  }
  if (!$op) {
    return;
  }
  // make sure that article_id is not missing
  if ($op!='POST') {
    if ($doaj_info['article_id']=='') {
      drupal_set_message(t('Error: article_id is missing from the database. What can you do when you see this? Delete and re-create the node, plus delete the article from the DOAJ website manually.'), 'error');
    }
  }

  // construct url
  $url .= '/articles';
  if ($doaj_info['article_id']!='') {
    $url .= '/' . $doaj_info['article_id'];
  }
  $url .= '?api_key=' . $apikey;

  // HTTP request
  $headers=array();
  if ($op!='DELETE') {
    $headers['Content-Type'] ='application/json; charset=UTF-8';
    // get JSON
    $node_json_warnings=array();
    $json = _doaj_connector_get_node_json($node, $node_json_warnings);
    if (!$json) {
      drupal_set_message(t('Failed to generate DOAJ JSON'), 'error');
      if ($node_json_warnings) {
        foreach ($node_json_warnings as $node_json_warning) {
          drupal_set_message($node_json_warning);
        }
      }
      return;
    }
  }
  $headers['Accept'] ='application/json; charset=UTF-8';
  $options = array(
    'headers' => $headers,
    'timeout' => variable_get('http_request_timeout', 30),
    'method' => $op,
    'data' => (($op=='DELETE') || ($op=='GET')) ? '' : $json,
  );
  $result=drupal_http_request($url, $options);
  $result_code=0;
  $result_text='[' . $op . '] ';
  if (is_object($result)) {
    if (isset($result->code)) {
      $result_code=$result->code;
    }
    if ($result_code==204) {
      // 'OK' looks better in the log than 'NO CONTENT'
      $result->status_message = 'OK';
    }
    if (isset($result->status_message)) {
      $result_text .= $result->status_message . '. ';
    }
    if (isset($result->error) && (!isset($result->status_message) || ($result->status_message!=$result->error))) {
      $result_text .= $result->error . '. ';
    }
    // convert response to JSON
    if (!empty($result->data)) {
      $data_json=json_decode($result->data, $assoc=TRUE, $depth=512);
      if ($data_json==NULL) {
        $result_text .= 'JSON error code ' . json_last_error() . '. ';
      }
      else {
        if (!empty($data_json['status'])) {
          $result_text .= 'DOAJ status:' . $data_json['status'] . '. ';
        }
        if (!empty($data_json['error'])) {
          $result_text .= 'DOAJ error: ' . $data_json['error'] . '. ';
        }
        if (($op='POST') && !empty($data_json['id'])) {
          $doaj_info['article_id']=$data_json['id'];
        }
      }
    }
  }
  else {
    $result_text .= 'Internal error';
  }
  // Add upload result to the log
  $id = db_insert('doaj_connector_node_log')
  ->fields(array(
    'nid' => $node->nid,
    'node_revision' => 0, // this is no longer used
    'node_timestamp' => 0, // this is no longer used
    'upload_timestamp' => time(),
    'upload_uid' => $user->uid,
    'availability' => $availability,
    'result_code' => $result_code,
    'result_text' => check_plain($result_text),
  ))
  ->execute();

  // If upload was successful, save info
  if (($result_code==200) || ($result_code==201) || ($result_code==204)) {
    $doaj_info['availability']=$availability;
    if ($op=='DELETE') {
      $doaj_info['article_id']='';
    }
    if ($op=='DELETE') {
      $doaj_info['node_json']='';
    }
    elseif ($op=='GET') {
      // no change
    }
    else {
      $doaj_info['node_json']=$json;
    }
    _doaj_connector_set_doaj_info($doaj_info);
    drupal_set_message(t('Operation was successful.'));
  }
  else {
    drupal_set_message(t('Operation failed, please check the log below.'));
  }
}

/**
 * Show current status of the doaj-enabled nodes
 */
function doaj_connector_doaj_status_page() {
  drupal_set_title(t('DOAJ status'));
  // check settings
  $apikey=trim(variable_get('doaj_connector_access_apikey', ''));
  if ($apikey=="") {
    drupal_set_message(t('Please configure the API Key.'), 'error');
  }
  if (!_doaj_connector_include_sitespecific()) {
    drupal_set_message(t('Please check the site-specific configuration.'), 'error');
  }
  $output = '';
  $header = array(t('Title'), t('Reason for update'));
  $rows = array();
  $header2 = array(t('Title'), t('Article ID'));
  $rows2 = array();
  // get configured content types
  $content_types=variable_get('doaj_connector_content_types', array());
  if ($content_types) {
    // keep only the enabled content types in the array
    $content_types=array_filter($content_types);
  }

  // get all the nodes of the configured content types
  if ($content_types) {
    $result = db_query('SELECT nid, vid, title, type, status, changed FROM {node} WHERE type IN (:type) ORDER BY changed DESC', array(':type' => $content_types));
    $output .= t('Enabled content types: @list (!change)', array('@list' => implode(', ', $content_types), '!change' => l(t('change configuration'), 'admin/config/content/doaj_connector')));
    $output .='<br />';
  }
  else {
    $result = db_query('SELECT nid, vid, title, type, status, changed FROM {node} ORDER BY changed DESC');
    $output .= t('Enabled content types: @list (!change)', array('@list' => t('All'), '!change' => l(t('change configuration'), 'admin/config/content/doaj_connector')));
    $output .='<br />';
  }
  foreach ($result as $record) {
    $doaj_info=_doaj_connector_get_doaj_info($record->nid);
    if ($doaj_info['availability']==DOAJ_CONNECTOR_AVAILABILITY_NONE) {
      if ($record->status) {
        $rows[]=array(
          l($record->title, 'node/' . $record->nid . '/doaj_connector'),
          t('Not uploaded yet.'),
        );
      }
    }
    elseif ($doaj_info['availability']==DOAJ_CONNECTOR_AVAILABILITY_UPLOADED) {
      $node_json_warnings=array();
      if (!$record->status) {
        $rows[]=array(
          l($record->title, 'node/' . $record->nid . '/doaj_connector'),
          t('Should be marked as "Not available".'),
        );
      }
      elseif ($doaj_info['node_json']!=_doaj_connector_get_node_json(node_load($record->nid), $node_json_warnings)) {
        $rows[]=array(
          l($record->title, 'node/' . $record->nid . '/doaj_connector'),
          t('Changes detected'),
        );
      }
      else {
        // check whether last operation was successful
        $result2 = db_query_range('SELECT nid, result_code FROM {doaj_connector_node_log} WHERE nid=:nid ORDER BY upload_timestamp DESC', 0, 1, array(':nid' => $record->nid));
        foreach ($result2 as $record2) {
          if (($record2->result_code!=200) && ($record2->result_code!=201) && ($record2->result_code!=204)) {
            $rows[]=array(
              l($record->title, 'node/' . $record->nid . '/doaj_connector'),
              t('Last operation failed.'),
            );
          }
          // this node needs no update
          else {
            $rows2[]=array(
              l($record->title, 'node/' . $record->nid . '/doaj_connector'),
              l($doaj_info['article_id'], 'https://doaj.org/article/' . $doaj_info['article_id']),
            );
          }
        }
      }
    }
    elseif ($doaj_info['availability']==DOAJ_CONNECTOR_AVAILABILITY_REMOVED) {
      if ($record->status) {
        $rows[]=array(
          l($record->title, 'node/' . $record->nid . '/doaj_connector'),
          t('Should be marked as "Available".'),
        );
      }
    }
  }
  if (count($rows)) {
    $output .= theme('table', array('header' => $header, 'rows' => $rows, 'caption' => t('Nodes that need update')));
  }
  else {
    $output .= t('No update is necessary.');
  }
  if (count($rows2)) {
    $output .= theme('table', array('header' => $header2, 'rows' => $rows2, 'caption' => t('Uploaded nodes')));
  }
  return $output;

}

/**
 * Show JSON content on the /node/%node/doaj_connector.json page
 */
function doaj_connector_page_show_json($node) {
  if (_doaj_connector_is_content_type_enabled($node->type)) {
    $node_json_warnings=array();
    $json=_doaj_connector_get_node_json($node, $node_json_warnings);
    if ($json) {
      drupal_add_http_header('Content-Type', 'application/json');
      echo $json;
      drupal_exit();
    }
  }
  return t('Failed to get the JSON');
}

/**
 * Show old JSON content on the /node/%node/doaj_connector.old.json page
 */
function doaj_connector_page_show_old_json($node) {
  if (_doaj_connector_is_content_type_enabled($node->type)) {
    $doaj_info=_doaj_connector_get_doaj_info($node->nid);
    $json=$doaj_info['node_json'];
    if ($json) {
      drupal_add_http_header('Content-Type', 'application/json');
      echo $json;
      drupal_exit();
    }
  }
  return t('Failed to get the JSON');
}

