<?php
/**
 * @file
 *  Site-specific include file for doaj_connector.module.
 *
 *  This file is included only if it is configured that way.
 */

/*
This example.sitespecific.inc file is provided only as an example.

Since this file uses fields that are specific to the IPR site,
for which this module was developed, you will need to customize it for your own site.
Instead of editing this file directly, follow these steps:
- determine a machine name that identifies your site, let's say "yoursite".
- copy this file, keeping the .sitespecific.inc extension, let's say "yoursite.sitespecific.inc"
- go to the config page at admin/config/content/doaj_connector
- enter the machine name to include the new file
- edit the new file according to the fields used on your site
- remove this example-related comment block from your new file

To help you customize this file, here is some IPR-related info.

IPR Node fields used in this file:
field_ref_news_user (user reference)
field_article_issue (term reference)
field_anlaysis_txt_abstract (text field)
field_comp_volumes (computd field)
field_comp_doi (computed field)

IPR User fields used in this file:
field_user_firstname
field_user_lastname
field_user_institution

IPR EISSN fixed value is used in this file.
*/
 
/**
 * Get node content as JSON that can be uploaded to DOAJ
 * This is a site-specific function
 */
function _doaj_connector_get_node_json_sitespecific($node, &$return_warnings) {
  // authors are expected in array of arrays, with keys 'name', 'email', 'affiliation'
  $data_authors = array();
  if ($data_author_items = field_get_items('node', $node, 'field_ref_news_user')) {
    foreach ($data_author_items as $data_author_item) {
      if (!empty($data_author_item['uid'])) {
        if ($data_author=user_load($data_author_item['uid'])) {
          $data_info=array('name' => '', 'email' => '', 'affiliation' => '');
          // take the difficult road and do not assume that the firstname/lastname is always there
          if (($data_item = field_get_items('user', $data_author, 'field_user_firstname')) && !empty($data_item[0]["value"])) {
            $data_info['name'] = $data_item[0]["value"];
            if (($data_item = field_get_items('user', $data_author, 'field_user_lastname')) && !empty($data_item[0]["value"])) {
              $data_info['name'] .= " " . $data_item[0]["value"];
            }
          }
          elseif (($data_item = field_get_items('user', $data_author, 'field_user_lastname')) && !empty($data_item[0]["value"])) {
            $data_info['name'] = $data_item[0]["value"];
          }
          if (($data_item = field_get_items('user', $data_author, 'field_user_institution')) && !empty($data_item[0]["value"])) {
            $data_info['affiliation'] = $data_item[0]["value"];
          }
          $data_info['email'] = $data_author->mail;
          $data_authors[]=$data_info;
        }
      }
    }
  }
  // keywords are expected as an array, not as a string
  $data_keywords = array();
  $data_keyword_max=6; // DOAJ keywords may only contain a maximum of 6 keywords
  if ($data_keyword_items = field_get_items('node', $node, 'field_article_issue')) {
    if (!is_null($return_warnings) && (count($data_keyword_items)>6)) {
      $return_warnings[]=t('Warning: This article has more than 6 keywords, DOAJ supports only 6.');
    }
    foreach ($data_keyword_items as $data_keyword_item) {
      if (isset($data_keyword_item['taxonomy_term']) && ($data_keyword_term=$data_keyword_item['taxonomy_term']) && isset($data_keyword_term->name)) {
        $data_keywords[]=$data_keyword_term->name;
        $data_keyword_max--;
        if (!$data_keyword_max) {
          break;
        }
      }
    }
  }
  $data_abstract = '';
  if ($data_abstract_items = field_get_items('node', $node, 'field_anlaysis_txt_abstract')) {
    foreach ($data_abstract_items as $data_abstract_item) {
      if (isset($data_abstract_item['safe_value'])) {
        // DOAJ does not support HTML tags in abstract
        $data_abstract=trim(strip_tags($data_abstract_item['safe_value']));
        break;
      }
    }
  }
  // Load data from field_comp_volumes
  $data_volume='';
  $data_issue='';
  if ($data_comp_items = field_get_items('node', $node, 'field_comp_volumes')) {
    $data_issue = $data_comp_items[4]['value'];
    $data_volume = $data_comp_items[6]['value'];
  }
  $data = array(
    'bibjson' => array(
      'author' => $data_authors,
      'abstract' => $data_abstract,
      'title' => $node->title,
      'year' => format_date($node->created, 'custom', 'Y'),
      'month' => format_date($node->created, 'custom', 'n'),
      'link' => array(
        array(
          'url' => url('node/' . $node->nid, array('absolute' => TRUE, 'alias' => TRUE)),
          'type' => 'fulltext',
          'content_type' => 'HTML',
        ),
      ),
      'keywords' => $data_keywords,
      'identifier' => array(
        // Note: DOI ID is inserted here later, if available
        // Note: EISSN is a fixed value
        array(
          'type' => 'eissn',
          'id' => '2197-6775',
        ),
      ),
      'journal' => array(
        'number' => $data_issue,
        'volume' => $data_volume,
      ),
    ), // 'bibjson'
  ); // $data

  // Insert DOI ID, if available
  $data_doi='';
  if ($data_doi_items = field_get_items('node', $node, 'field_comp_doi')) {
    foreach ($data_doi_items as $data_doi_item) {
      if (isset($data_doi_item['value'])) {
        $data_doi=$data_doi_item['value'];
      }
    }
  }

  if ($data_doi!='') {
    $data['bibjson']['identifier'][]=array(
      'type' => 'doi',
      'id' => $data_doi,
    );
  }
  return json_encode($data, JSON_HEX_QUOT | JSON_HEX_TAG | JSON_HEX_AMP | JSON_HEX_APOS | JSON_PRETTY_PRINT);

}

